-- DROP SCHEMA email ;
CREATE SCHEMA email;

-- DROP TABLE email.email_templates;
CREATE TABLE email.email_templates
(
    id bigserial,
    name character varying(512), 
    subject character varying(512),
    type character varying(128),
    template character varying,
    CONSTRAINT email_template_pkey PRIMARY KEY (id)
);

CREATE TABLE email.tasks
(
    id bigserial,
    task_type character varying(64), 
    details character varying,
    init_timestamp timestamp without time zone,
    end_timestamp timestamp without time zone,
    CONSTRAINT email_tasks_pkey PRIMARY KEY (id)
);