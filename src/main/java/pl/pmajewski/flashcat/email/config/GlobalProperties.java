package pl.pmajewski.flashcat.email.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GlobalProperties {
	
	public static String unreplayName;
	
	public static String unreplayPersonalName;

	@Value("${spring.mail.username}")
	public void setUnreplayName(String value) {
		GlobalProperties.unreplayName = value;
	}
	
	@Value("${spring.mail.from}")
	public void setUnreplayPersonalName(String value) {
		GlobalProperties.unreplayPersonalName = value;
	}
}
