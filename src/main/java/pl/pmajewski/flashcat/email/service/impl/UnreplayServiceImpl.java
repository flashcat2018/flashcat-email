package pl.pmajewski.flashcat.email.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.email.config.GlobalProperties;
import pl.pmajewski.flashcat.email.dto.EmailBody;
import pl.pmajewski.flashcat.email.dto.TemplateEmailBody;
import pl.pmajewski.flashcat.email.exception.EmailSendingException;
import pl.pmajewski.flashcat.email.exception.TemplateNotFoundException;
import pl.pmajewski.flashcat.email.model.EmailTemplate;
import pl.pmajewski.flashcat.email.model.Task.TaskType;
import pl.pmajewski.flashcat.email.repository.EmailTemplateRepository;
import pl.pmajewski.flashcat.email.service.UnreplayService;
import pl.pmajewski.flashcat.email.task.AsyncTask;

@Service
@CommonsLog
public class UnreplayServiceImpl implements UnreplayService {

	@Autowired
	private JavaMailSender mailSender;

	private EmailTemplateRepository templateRepository;
	
	public UnreplayServiceImpl(EmailTemplateRepository templateRepository) {
		this.templateRepository = templateRepository;
	}

	@Override
	@AsyncTask(taskType = TaskType.UNREPLAY_BY_HTML)
	public void send(EmailBody body)  {
		sendEmail(body.to, body.subject, body.content);
	}

	@Override
	@AsyncTask(taskType = TaskType.UNREPLAY_BY_TEMPLATE)
	public void send(String templateName, TemplateEmailBody body) {
		EmailTemplate template = templateRepository.findByName(templateName)
				.orElseThrow(() -> new TemplateNotFoundException("EmailTemplate with name = "+templateName+" not found"));
		sendEmail(body.to, template.getSubject(), replaceParams(template.getTemplate(), body.params));
	}
	
	private String replaceParams(String templateContent, Map<String, String> params) {
		Set<Entry<String, String>> entrySet = params.entrySet();
		for (Entry<String, String> param : entrySet) {
			templateContent = templateContent.replaceAll("\\$\\{"+param.getKey()+"\\}", param.getValue());
		}
		return templateContent;
	}

	private void sendEmail(String to, String subject, String content) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
			String htmlMsg = content;
			mimeMessage.setContent(htmlMsg, "text/html");
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setFrom(GlobalProperties.unreplayName, GlobalProperties.unreplayPersonalName);
			mailSender.send(mimeMessage);
			log.info("[SEND] [SUCCESS] ["+to+"] ["+content+"]");
		} catch (MessagingException | UnsupportedEncodingException e) {
			log.error("[SEND] [FAIL] ["+to+"] ["+content+"]");
			throw new EmailSendingException(e);
		}
	}
}
