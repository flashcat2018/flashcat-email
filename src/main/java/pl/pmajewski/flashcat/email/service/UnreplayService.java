package pl.pmajewski.flashcat.email.service;

import pl.pmajewski.flashcat.email.dto.EmailBody;
import pl.pmajewski.flashcat.email.dto.TemplateEmailBody;

public interface UnreplayService {
	
	void send(EmailBody body);
	
	void send(String templateName, TemplateEmailBody body);
}
