package pl.pmajewski.flashcat.email.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.email.dto.EmailTemplateDTO;
import pl.pmajewski.flashcat.email.model.EmailTemplate;
import pl.pmajewski.flashcat.email.repository.EmailTemplateRepository;
import pl.pmajewski.flashcat.email.service.EmailTemplateService;

@Service
@Transactional
public class EmailTemplateServiceImpl implements EmailTemplateService {

	private EmailTemplateRepository emailRepo;
	
	public EmailTemplateServiceImpl(EmailTemplateRepository emailRepo) {
		this.emailRepo = emailRepo;
	}

	@Override
	public List<EmailTemplateDTO> findAll() {
		 return emailRepo.listAll().map(EmailTemplate::getPojo).collect(Collectors.toList());
	}
}
