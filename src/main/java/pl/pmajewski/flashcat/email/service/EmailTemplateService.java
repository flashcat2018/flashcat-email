package pl.pmajewski.flashcat.email.service;

import java.util.List;

import pl.pmajewski.flashcat.email.dto.EmailTemplateDTO;

public interface EmailTemplateService {

	List<EmailTemplateDTO> findAll();
}
