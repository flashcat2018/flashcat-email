package pl.pmajewski.flashcat.email.service;

import java.util.List;

import pl.pmajewski.flashcat.email.dto.TaskDTO;
import pl.pmajewski.flashcat.email.model.Task;
import pl.pmajewski.flashcat.email.model.Task.TaskType;

public interface TaskService {
	
	TaskDTO initTask(TaskType type, String details);
	
	void endTask(Long taskId);
	
	List<Task> listWaiting();
}
