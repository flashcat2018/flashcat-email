package pl.pmajewski.flashcat.email.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.email.dto.TaskDTO;
import pl.pmajewski.flashcat.email.exception.TaskNotFoundException;
import pl.pmajewski.flashcat.email.model.Task;
import pl.pmajewski.flashcat.email.model.Task.TaskType;
import pl.pmajewski.flashcat.email.repository.TaskRepository;
import pl.pmajewski.flashcat.email.service.TaskService;

@Service
public class TaskServiceImpl implements TaskService {

	private TaskRepository taskRepo;

	public TaskServiceImpl(TaskRepository taskRepo) {
		this.taskRepo = taskRepo;
	}

	@Override
	public TaskDTO initTask(TaskType type, String details) {
		Task task = new Task(type, details);
		task = taskRepo.save(task);
		return task.getTaskDTO();
	}

	@Override
	@Transactional
	public void endTask(Long taskId) {
		Task task = taskRepo.findById(taskId)
				.orElseThrow(() -> new TaskNotFoundException("Task with id: "+taskId+" not found"));
		task.setEndTimestamp(LocalDateTime.now());
	}

	@Override
	public List<Task> listWaiting() {
		return taskRepo.findWaiting();
	}
}
