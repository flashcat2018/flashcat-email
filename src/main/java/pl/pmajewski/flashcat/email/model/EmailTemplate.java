package pl.pmajewski.flashcat.email.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pl.pmajewski.flashcat.email.dto.EmailTemplateDTO;

@Entity
@Table(name = "email_templates", schema = "email")
@EqualsAndHashCode(of = "id")
@Data
@NoArgsConstructor
public class EmailTemplate {

	public enum TemplateType{ HTML }
	
	public EmailTemplate(String name, TemplateType type, String template) {
		this.name = name;
		this.type = type;
		this.template = template;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 512)
	private String name;

	@Column(length = 512)
	private String subject;
	
	@Column(length = 128, nullable = false)
	@Enumerated(EnumType.STRING)
	private TemplateType type;
	
	@Column
	private String template;
	
	public EmailTemplateDTO getPojo() {
		EmailTemplateDTO p = new EmailTemplateDTO();
		
		p.id = id;
		p.name = name;
		p.type = type.toString();
		p.template = template;
		p.variables = findVariables();
		
		return p;
	}
	
	private List<String> findVariables() {
		if(this.template == null) {
			return new ArrayList<>();
		}
		
		List<String> result = new ArrayList<>();
		Pattern pattern = Pattern.compile("\\$\\{(.*?)\\}");
		Matcher matcher = pattern.matcher(this.template);
		while (matcher.find()) {
		    result.add(matcher.group(1));
		}
		
		return result;
	}
}
