package pl.pmajewski.flashcat.email.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pl.pmajewski.flashcat.email.dto.TaskDTO;

@Table(schema = "email", name = "tasks")
@Entity
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Task {
	
	public enum TaskType { UNREPLAY_BY_TEMPLATE, UNREPLAY_BY_HTML }
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column(name = "task_type")
	@Enumerated(EnumType.STRING)
	private TaskType taksType;

	@Column
	private String details;
	
	@Column(name = "init_timestamp")
	private LocalDateTime initTimestamp;
	
	@Column(name = "end_timestamp")
	private LocalDateTime endTimestamp;

	public Task(TaskType type, String details) {
		this.taksType = type;
		this.details = details;
		initTimestamp = LocalDateTime.now();
	}
	
	public TaskDTO getTaskDTO() {
		TaskDTO dto = new TaskDTO();
		dto.id = id;
		return dto;
	}
}
