package pl.pmajewski.flashcat.email.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.pmajewski.flashcat.email.dto.EmailTemplateDTO;
import pl.pmajewski.flashcat.email.service.EmailTemplateService;

@RestController
@RequestMapping("templates")
public class TemplateController {

	private EmailTemplateService templateService;

	public TemplateController(EmailTemplateService templateService) {
		this.templateService = templateService;
	}
	
	@GetMapping
	public List<EmailTemplateDTO> listAll() {
		return templateService.findAll();
	}
	
}
