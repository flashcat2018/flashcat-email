package pl.pmajewski.flashcat.email.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.email.dto.EmailBody;
import pl.pmajewski.flashcat.email.dto.TemplateEmailBody;
import pl.pmajewski.flashcat.email.service.UnreplayService;

@RestController
@CommonsLog
public class EmailController {
	
	private UnreplayService unreplayService;
	
	public EmailController(UnreplayService unreplayService) {
		this.unreplayService = unreplayService;
	}

	@PostMapping(value = "/email/send")
	public void sendEmail(@RequestBody EmailBody body) {
		log.info("[SEND] ["+body.to+"] ["+body+"]");
		this.unreplayService.send(body);
	}
	
	@PostMapping(value = "/email/send/template/{templateName}")
	public void sendEmail(@PathVariable String templateName, @RequestBody TemplateEmailBody body) {
		unreplayService.send(templateName, body);
	}
}
