package pl.pmajewski.flashcat.email.task;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import pl.pmajewski.flashcat.email.model.Task.TaskType;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AsyncTask {

	TaskType taskType();
}
