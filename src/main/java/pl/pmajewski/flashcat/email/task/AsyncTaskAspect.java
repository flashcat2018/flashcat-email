package pl.pmajewski.flashcat.email.task;

import java.lang.reflect.Method;
import java.util.concurrent.CompletableFuture;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.email.dto.TaskDTO;
import pl.pmajewski.flashcat.email.model.Task.TaskType;
import pl.pmajewski.flashcat.email.service.TaskService;
import pl.pmajewski.flashcat.email.utils.JsonMapper;

@Aspect
@Component
@CommonsLog
public class AsyncTaskAspect {

	private TaskService taskService;
	
	public AsyncTaskAspect(TaskService taskService) {
		this.taskService = taskService;
	}

	@Around("@annotation(AsyncTask)")
	public void process(ProceedingJoinPoint joinPoint) throws Throwable {
		TaskDTO task = taskService.initTask(extractTaskType(joinPoint), extractDetails(joinPoint));
		CompletableFuture.supplyAsync(() -> {
				try {
					return joinPoint.proceed();
				} catch (Throwable e) {
					throw new RuntimeException(e);
				}
		}).thenApply(result -> {
			taskService.endTask(task.id);
			return result;
		}).exceptionally(e -> {
			log.error(e.getMessage(), e);
			return null;
		});
	}
	
	private String extractDetails(ProceedingJoinPoint joinPoint) {
		Object[] args = joinPoint.getArgs();
		StringBuilder sb = new StringBuilder();
		for (Object item : args) {
			sb.append("[");
			sb.append(JsonMapper.mapToString(item));
			sb.append("] ");
		}
		return sb.toString();
	}
	
	private TaskType extractTaskType(ProceedingJoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
	    Method method = signature.getMethod();

	    AsyncTask myAnnotation = method.getAnnotation(AsyncTask.class);
	    return myAnnotation.taskType();
	}
}
