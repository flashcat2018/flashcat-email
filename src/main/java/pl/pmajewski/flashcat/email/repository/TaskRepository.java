package pl.pmajewski.flashcat.email.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import pl.pmajewski.flashcat.email.model.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {
	
	@Query("select t from Task t where t.endTimestamp is null")
	List<Task> findWaiting();
	
}
