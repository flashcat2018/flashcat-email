package pl.pmajewski.flashcat.email.repository;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import pl.pmajewski.flashcat.email.model.EmailTemplate;

public interface EmailTemplateRepository extends CrudRepository<EmailTemplate, Long> {

	@Query("from EmailTemplate")
	Stream<EmailTemplate> listAll();
	
	Optional<EmailTemplate> findByName(String name);
}
