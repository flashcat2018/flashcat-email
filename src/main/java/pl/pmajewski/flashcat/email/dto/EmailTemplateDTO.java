package pl.pmajewski.flashcat.email.dto;

import java.util.List;

public class EmailTemplateDTO {

	public Long id;
	public String name;
	public String type;
	public List<String> variables;
	public String template;
	
}
