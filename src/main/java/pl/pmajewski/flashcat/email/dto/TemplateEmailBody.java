package pl.pmajewski.flashcat.email.dto;

import java.util.Map;

import lombok.ToString;

@ToString
public class TemplateEmailBody {

	public String to;
	public Map<String, String> params;
}
