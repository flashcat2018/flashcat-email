package pl.pmajewski.flashcat.email.dto;

import lombok.ToString;

@ToString
public class EmailBody {
	
	public String to;
	public String subject;
	public String content;
}
