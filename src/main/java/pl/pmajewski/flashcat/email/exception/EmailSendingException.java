package pl.pmajewski.flashcat.email.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Unexpected email sending error")
public class EmailSendingException extends RuntimeException {

	public EmailSendingException() {
		super();
	}

	public EmailSendingException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public EmailSendingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public EmailSendingException(String arg0) {
		super(arg0);
	}

	public EmailSendingException(Throwable arg0) {
		super(arg0);
	}
}
