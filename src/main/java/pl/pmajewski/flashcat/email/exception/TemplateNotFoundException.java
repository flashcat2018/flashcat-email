package pl.pmajewski.flashcat.email.exception;

public class TemplateNotFoundException extends RuntimeException {

	public TemplateNotFoundException(String message) {
		super(message);
	}
}
