package pl.pmajewski.flashcat.email.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonMapper {

	public static String mapToString(Object object) {
		try {
			ObjectMapper objMapper = new ObjectMapper();
			return objMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
}
