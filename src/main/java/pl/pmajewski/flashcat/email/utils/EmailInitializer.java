package pl.pmajewski.flashcat.email.utils;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import pl.pmajewski.flashcat.email.model.Task.TaskType;
import pl.pmajewski.flashcat.email.service.TaskService;
import pl.pmajewski.flashcat.email.service.UnreplayService;

@Component
public class EmailInitializer {

	private UnreplayService unreplayService;
	private TaskService taskService;

	public EmailInitializer(UnreplayService unreplayService, TaskService taskService) {
		this.unreplayService = unreplayService;
		this.taskService = taskService;
	}
	
	@PostConstruct
	public void queueTasks() {
		taskService.listWaiting()
			.forEach(i -> {
				if(TaskType.UNREPLAY_BY_TEMPLATE.equals(i.getTaksType())) {
					// TODO Add resurection
				}
			});
	}
}
